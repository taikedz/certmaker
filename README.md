CertMaker
===========

A tool to create an OpenSSL certificate authority, and generate certificates, for use on internal networks.

Written prior to discovering [Let's Encrypt's Boulder project](https://github.com/letsencrypt/boulder). A much more sensible option.
